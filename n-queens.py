from pprint import pprint


n = int(input("Enter the number of queens "))
# create the board places using n
board = [[0]*n for _ in range(n)]

def is_it_safe(row, column):
    for space in range(len(board)):
        if  board[row][space] == 1 or board[space][column] == 1:
            return False
# check diagonals
    for space in range(len(board)):
        for diagonal in range(len(board)):
            if (space + diagonal == row + column) or (space - diagonal == row - column):
                if board[space][diagonal] == 1:
                    return False
    return True

def find_positions(n):
    if n == 0:
        return True
    for row in range(len(board)):
        for column in range(len(board)):
            if (is_it_safe(row, column)) and (board[row][column] != 1):
                board[row][column] = 1
                if find_positions(n-1) == True:
                    return True
                board[row][column] = 0
    return False



find_positions(n)
for space in board:
    pprint(space)